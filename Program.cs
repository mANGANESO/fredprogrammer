﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practica_de_clase
{
    class Program
    {
        static void Main(string[] args)
        {
            int a;
            Console.WriteLine("Buenos días, ingrese el número indicado para acceder al programa \n" +
             "1) PROGRAMA PARA VER LAS TABLAS DE MULTIPLICAR \n" +
             "2) PROGRAMA QUE CORRE CON DECIMALES \n" +
             "3) PROGRAMA PARA ACCEDER CON SU USUARIO Y CONTRASEÑA ");
            a = int.Parse(Console.ReadLine());
            switch (a)
            {
                case 1:
                    Class1.Project();
                    break;

                case 2:
                    Class1.Numbers();
                    break;
                case 3:
                    Class1.codigo();
                    break;
            }
            Console.ReadKey();
        }
    }
}
